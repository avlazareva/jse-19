package ru.t1.lazareva.tm.command;

import ru.t1.lazareva.tm.api.model.ICommand;
import ru.t1.lazareva.tm.api.service.IServiceLocator;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";

        final boolean hasName = name != null && !name.isEmpty();
        final boolean hasArgument = argument != null && !argument.isEmpty();
        final boolean hasDescription = description != null && !description.isEmpty();

        if (hasName) result += name;
        if (hasArgument) result += hasName ? ", " + argument : argument;
        if (hasDescription) result += ": " + description;
        return result;
    }

}