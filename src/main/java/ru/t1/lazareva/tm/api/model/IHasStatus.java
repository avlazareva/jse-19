package ru.t1.lazareva.tm.api.model;

import ru.t1.lazareva.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}